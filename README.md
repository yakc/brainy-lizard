Brainy Lizard
=============

Exploration on how to craft properly a robot compute layer with two types of compute: Lizard level and higher level.

The goal is to find a robust way to have an autonomous robot function longer, with reliable degraded modes. When reviewing the capabilities of a robot, we can usually identify "core" functionalities strictly necessary for the robot to conserve its integrity, and extras. For example a mobile robot may be able to track its position and recognise objects. Mobility-related capabilities (e.g. motor control) are part of the core as _raison d'etre_ together with localisation to know where it is, and where its charging station may be. Recognising object is necessary to complete its mission, but not core to its integrity---an extra.

Toward this goal, this repository proposes to put all core functionalities on a dedicated, highly reliable and secure compute, and extras on a secondary compute, perhaps less reliable (but also more powerful, e.g. for ML capabilities). If the core compute fails, the whole robot halts. If the extra compute fails, the robot is (as an aim) able to fallback automatically on its core compute, with a default mission to, say, return to its charging station and send an alert.

This also allows finer-grained power management. The core compute can implement a routine to turn on and off the extra compute depending on the mission, and depending on actual power availability. Doing so the robot can autonomously manage its energy, typically by turing off extra compute and fall back to its default mission to return to the charging station.

This goal is for now an architecture with a core and an extra compute. There are efforts to keep the architecture flexible enough to get a core manage several extra computes too.

Setup
-----

### Requirements

* A recent Bash, as in the last 10 years.
* Right now working with MacPorts on MacOS. The underlying libraries support Linux and Windows, but not hooked nor tested yet. Homebrew is ongoing and may come earlier than others.
* ESP32-DevKitCv4 or compatible for the lizard level.
* RPi4 model B for the higher level.

### First time install

Automated with scripts and `make`, the procedure is in `bin/install`. If your target local environment is not yet supported, the easiest may be to just adapt dependency management commands in this file (the rest tries to end with a canned installation---clean and easy to wipe---although Windows will require more work).

    make install

The procedure takes some time and should end with a complete message and a `source` command. This command is necessary in each shell you want the rest of the software to run. It can be interesting to add the command to `~/.bashrc` or equivalent.

### Hardware settings

#### ESP32

1. Please make sure the board is working when connected via USB to the computer where this repository is configured. Working means the [serial connection](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/establish-serial-connection.html#establish-serial-connection-with-esp32) is usable with the board.
